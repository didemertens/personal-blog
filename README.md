<h1 align="center">
  Gatsby Blog
</h1>

## 🦾 Built with
* Gatsby
* GitLab
* Netlify CMS

## 🚀Quick start
  Clone or download the repo. Install all the packages with npm:
   ```shell
   npm install
  ```

   ### Run the server:
  ```shell
  gatsby develop
  ```
  Your site is now running at `http://localhost:8000`! 🥳

  If you get an error, make sure you have the Gatsby CLI globally installed:
  ```shell
  npm install -g gatsby-cli
  ```

  ### GitLab CI/CD
  Change site meta data (for CI/CD) in gatsby-config.js to your own name/repo.
  ```shell
  module.exports = {
    siteMetadata: {
      title: `Dide's blog`,
      description: `This is Dide's personal blog.`,
      author: `@gatsbyjs`,
    },
    pathPrefix: `/personal-blog`
  }
  ```

  Then push your changes and that's it! If you click on CI/CD > Pipelines on GitLab you can see the pipeline for your commit.

  Click [here](https://www.gatsbyjs.org/docs/deploying-to-gitlab-pages/) if you want more info on setting up CI/CD for a Gatsby project.

  Your project is deployed and can be found on `https://YOUR_GITLAB_USERNAME.gitlab.io/NAME_OF_YOUR_REPO` 🤩

  ### Netlify CMS:
  To use the Netlify CMS, you'll have to add your application in GitLab. 

  Go to GitLab > Settings > Applications and add a new application. Give it a name and a redirect URL (for example: https://YOUR_GITLAB_USERNAME.gitlab.io/NAME_OF_YOUR_REPO/admin/`). **Make sure you end your redirect URL with a '/'.**

  Tick the `api` box under scopes and click on save. After that, copy your Application ID and change `app_id` to your copied ID in the `static/admin/config.yml` file. Also don't forget to change the `repo` name here. 

  ```shell
  backend:
    name: gitlab
    repo: didemertens/personal-blog // change this
    auth_type: implicit
    app_id: ec8147350b91539ff436caaf72a1280b8b5c5b9e65c70e46c14ad52ba198c27a // and change this
  ...
  ```

  After that, go to `https://YOUR_GITLAB_USERNAME.gitlab.io/NAME_OF_YOUR_REPO/admin` and log in with your GitLab credentials. It might take a few minutes before your deployed project is updated and the CMS interface is available. 

  You can find more info about sourcing from the Netlify CMS with Gatsby [here](https://www.gatsbyjs.org/docs/sourcing-from-netlify-cms/). 




