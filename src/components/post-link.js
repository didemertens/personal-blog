import React from "react"
import { Link } from "gatsby"
import styled from "styled-components"

const LinkPost = styled.li`
  font-size: 22px;
  margin: 15px 0;
  color: black;
`

const PostLink = ({ post }) => (
  <LinkPost>
    &nbsp;
    <Link
    className="a--orange"
     to={`/${post.frontmatter.path}`}>
      {post.frontmatter.title} ({post.frontmatter.date})
    </Link>
  </LinkPost>
)
export default PostLink