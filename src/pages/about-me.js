import React from "react"
import Grid from '@material-ui/core/Grid'
import { makeStyles } from '@material-ui/core/styles'

import Layout from "../components/layout"
import SEO from "../components/seo"

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  card: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
    borderTop: `4px solid orange`,
    backgroundColor: `#f6f6f6`,
    borderRadius: `5px`
  },
  number: {
    textAlign: `left`,
    margin: 0
  }
}));


const AboutMe = () => {
  const classes = useStyles()

  return (
    <Layout>
      <SEO title="About Me" />
      <h1 style={{textAlign:`center`}}>Hi!</h1>
      <p>I'm Dide.</p>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit amet, consectetur adipisicing elit
        amet, consectetur adipisicing elit amet, consectetur adipisicing elit. amet, consectetur adipisicing elit.
        Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>

      <div className="curve-mask">
        <div className="curve-up" id="curve-2">
        <section className="curve-content-wrap">
              <div className="curve-content">
                <p className="curve-text">Lullington Church (1939) by James Bateman</p>
              </div>
            </section>
        </div>
      </div>

      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit amet, cons adipisicing elit
        amet, consectetur adipisicing elit amet, consectetur adipisicing elit. amet, consect.</p>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, pelit adipisicing elit
        amet, consectetur adipisicing elit amet, consectetur adipisicing elit. Amet, consectetur adipisicing elit
        consectetur adipisicing elit.</p>

      <Grid container spacing={3} className={classes.root}>
        <Grid container justify="center" item md>
          <div className={classes.card}>
            <header>
              <p className={classes.number}><span role="img" aria-label="Noodles">🍜</span></p>
              <h2 style={{margin: 0}}>Lorem</h2>
            </header>
            <p>
              Lorem ipsum dolor sit amet, consectetur adipisicing elit amet, consectetur adipisicing elit
              amet, consectetur adipisicing elit amet, consectetur adi.
            </p>
          </div>
        </Grid>
        <Grid container justify="center" item md>
        <div className={classes.card}>
          <header>
            <p className={classes.number}><span role="img" aria-label="Artist palette">🎨</span></p>
            <h2 style={{margin: 0}}>Ipsum</h2>
          </header>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit amet, consectetur adipisicing elit
            amet, consectetur adipisicing elit amet, consectetur adipisicing.
          </p>
        </div>
        </Grid>
        <Grid container justify="center" item md>
          <div className={classes.card}>
            <header>
              <p className={classes.number}><span role="img" aria-label="Books">📚</span></p>
              <h2 style={{margin: 0}}>Dolor</h2>
            </header>
            <p>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit amet, consectetur adipisicing elit
            amet, consectetur adipisicing elit amet, consectetur.
          </p>
          </div>
        </Grid>
      </Grid>
    </Layout>
  )
}

export default AboutMe
